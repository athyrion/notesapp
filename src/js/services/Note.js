angular.module('NotesApp.services.Note', [])
    .factory('Note', ['$http', function($http) {
        return {
            // GET call to fetch all notes
            get: function() {
                return $http.get('/api/notes');
            }

            // OTHER http verbs
        };
    }]);