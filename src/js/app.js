/**
 * The application's central file, where all external dependencies are
 * defined.
 *
 * @author Andreas Willems
 * @version 28 APR 2015
 */
angular.module('NotesApp', [
    'ngRoute',
    'mobile-angular-ui',
    'NotesApp.controllers.List',
    'NotesApp.services.Note'
]);