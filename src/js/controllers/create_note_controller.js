/**
 * Controller for the creation of a note.
 *
 * @author Andreas Willems
 * @version 28 APR 2015
 */
angular.module('NotesApp.controllers.CreateNote', [])

    .controller('CreateNoteController', ['$scope', 'Note',
        function($scope, Note) {

        }
    ]);

