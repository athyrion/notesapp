/**
 * Controller for the displaying the notes as a list.
 *
 * @author Andreas Willems
 * @version 28 APR 2015
 */
angular.module('NotesApp.controllers.List', [])

    .controller('ListController', ['$scope',
        function($scope){

            // create an array of notes for temporary notes
            var data = [
                {
                    noteId: 1,
                    noteTitle: 'Notiz eins',
                    noteContent: 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. ' +
                        'Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus ' +
                        'et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ' +
                        'ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis ' +
                        'enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In ' +
                        'enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis ' +
                        'eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper ' +
                        'nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat ' +
                        'vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a,'
                },
                {
                    noteId: 2,
                    noteTitle: 'Notiz zwei',
                    noteContent: 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. ' +
                    'Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus ' +
                    'et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ' +
                    'ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis ' +
                    'enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In ' +
                    'enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis ' +
                    'eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper ' +
                    'nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat ' +
                    'vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a,'
                },
                {
                    noteId: 3,
                    noteTitle: 'Notiz drei',
                    noteContent: 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. ' +
                    'Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus ' +
                    'et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ' +
                    'ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis ' +
                    'enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In ' +
                    'enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis ' +
                    'eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper ' +
                    'nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat ' +
                    'vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a,'
                }
            ];

            $scope.listOfNotes = data;

            $scope.getData = function() {
                /*Note.get().success(function(data) {
                    $scope.listOfNotes = data;
                });*/
                $scope.listOfNotes = data;

            };

            //$scope.getData();

        }]
    );