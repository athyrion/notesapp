/**
 * Definition of the application's front end routes.
 *
 * @author Andreas Willems
 * @version 28 APR 2015
 */
angular.module('NotesApp')

    // configure app to use html5Mode to avoid hashbang urls
    .config(['$locationProvider', function($locationProvider) {
        $locationProvider.html5Mode({
            enabled: true,
            requireBase: false
        });
    }])

    // configure front end routing
    .config(['$routeProvider', function($routeProvider) {

        $routeProvider
            .when('/', {
                templateUrl: 'list.html',
                reloadOnSearch: false,
                controller: 'ListController'
            })
            .when('/create', {
                templateUrl: 'create.html',
                reloadOnSearch: false,
                controller: 'CreateNoteController'
            })
            .when('/view/:id', {
                templateUrl: 'detail.html',
                reloadOnSearch: false,
                controller: 'ViewNoteController'
            });
    }]);